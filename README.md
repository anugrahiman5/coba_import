# Test-Driven Development

# Software Quality And Assurance

Software Quality Assurance adalah proses sistematis untuk memeriksa apakah sebuah
software telah dikembangkan sesuai dengan kebutuhan yang telah ditentukan sebelumnya. 
Proses ini, bisa dilaksanakan oleh seorang QA Tester atau oleh seorang QA Engineer

Repository ini berisi tentang latihan-latihan matakuliah Software Quality And Assurance


*  Buku referensi : 
https://www.obeythetestinggoat.com/book/praise.harry.html


*  **Untuk menjalankan gunakan perintah** <br>
`python manage.py runserver`



Untuk Project tiap latihan dapat diakses pada tautan berikut :

* [Latihan 1](https://github.com/anugrahiman5/Testing-Static-Web-Page)
* [Latihan 2](https://gitlab.com/anugrahiman5/test-driven-development/-/tree/exercise_2)
* [Latihan 3](https://gitlab.com/anugrahiman5/test-driven-development/-/tree/exercise_3)



## Tentang Excercise 1

Merupakan latihan dari Chapter 1-3

*  Unit Testing matematika sederhana

>     def test_bad_maths(self):
>         self.assertEqual(1 + 1, 3)


*  Unit testing untung mengecek html dan title
  
  ```
  def test_home_page_returns_correct_html(self):
        request = HttpRequest()  
        response = home_page(request)  
        html = response.content.decode('utf8')  
        self.assertTrue(html.startswith('<html>'))  
        self.assertIn('<title>To-Do lists</title>', html)  
        self.assertTrue(html.endswith('</html>')) 
```

## Tentang Exercise 2

Latihan 2 mengambil materi pada chapter 4 dan 5. Pada latihan ini sedikit 
membingungkan karena sering sekali kodenya error dan tidak sesuai dengan
apa yang diminta dari buku. Pada latihan 2 kali ini juga sudah menggunakan database

`inputbox.send_keys('Tugas terus')`

`inputbox.send_keys('Kerjakan 1 tumbuh seribu')`

Kode di atas yang digunakan untuk menambahkan komentar otomatis


## Cerita Exercise 3
Di Latihan 3, saya memperbaiki functional test dengan cara memastikan test isolation. **Test Isolation** adalah  proses pemecahan suatu sistem / aplikasi menjadi modul-modul yang lebih kecil dan berbeda sehingga dapat diuji atau dievaluasi dengan mudah. Jenis *testing* ini biasanya dilakukan ketika *bug* sulit ditemukan dan diselesaikan dalam suatu proyek perangkat lunak yang besar. Dengan bantuan *testing* ini, *programmer* dapat dengan mudah menemukan *error* dengan menjalankan *test* yang mengakibatkan masalah sistem / aplikasi.

Implementasi *test isolation* dalam latihan 3 bisa dilihat dari perubahan desainnya dari latihan 2 sebelumnya.
1. Mengganti parameter kelas FunctionalTest dari `unittest.Case` menjadi `LiveServerTestCase`. Kelas `LiveServerTestCase` akan membuat *test database* secara otomatis dan menjalankan suatu *development server* untuk menjalankan *functional testing*

**Sebelum** 

    import unittest
    ...
    class FunctionalTest(unittest.TestCase):
        def setUp(self):
        ...

**Sesudah**

    from django.test import LiveServerTestCase
    ...
    class FunctionalTest(LiveServerTestCase):
        def setUp(self):
        ...
    

2. `LiveServerTestCase` bisa dijalankan dengan Django *test runner* menggunakan `manage.py`. Berhubung Django memerlukan *package directory* yang valid, `functional_tests.py` dipindahkan dari *root directory* ke *package directory* baru yaitu `functional_test/` yang berisi `functional_test.py` diubah namanya menjadi `tests.py` dan file `__init__.py` baru. Direktorinya berubah menjadi seperti ini.

**Sebelum** 

        .
    ├── db.sqlite3
    ├── functional_tests.py
    ├── lists
        ├── [...]
    ├── manage.py
    ├── superlists
        ├── [...]
    └── Env
        ├── [...]

**Sesudah**

        .
    ├── db.sqlite3
    ├── functional_tests
    │   ├── __init__.py
    │   └── tests.py
    ├── lists
        ├── [...]
    ├── manage.py
    ├── superlists
        ├── [...]
    └── Env
        ├── [...]
3. Jika ingin menjalankan *unit test* dan *functional test* secara bersamaan, jalankan perintah `py manage.py test'
4. Jika ingin menjalankan *unit test*, jalankan perintah `py manage.py test lists`
5. Jika ingin menjalankan *functional test* jalankan perintah `py manage.py test functional_tests`

