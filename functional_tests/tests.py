from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import WebDriverException
import time
import unittest

MAX_WAIT = 10

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()

    def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()
        while True:
            try:
                table = self.browser.find_element_by_id('id_list_table')
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)

        # Untuk mengecek title dan h1
        self.assertIn('Anugrah Iman', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Anugrah Iman To-Do', header_text)

        # Untuk menggunakan baris 
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # menulis Tugas terus
        inputbox.send_keys('Tugas terus')

        #ketika selesai maka akan ditambahkan
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Tugas terus')

        #menambah lainnya
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Kerjakan 1 tumbuh seribu')
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: Tugas terus')
        self.wait_for_row_in_list_table('2: Kerjakan 1 tumbuh seribu')

        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        # self.assertTrue(
        #     any(row.text == '1: Buy peacock feathers' for row in rows),
        #     f"New to-do item did not appear in table. Contents were:\n{table.text}"
        # )
        # self.assertIn('1: Tugas terus', [row.text for row in rows])
        # self.assertIn(
        #     '2: Tak pernah dijelasin',
        #     [row.text for row in rows]
        # )
        # There is still a text box inviting her to add another item. She
        # enters "Use peacock feathers to make a fly" (Edith is very
        # methodical)
        # Cek
        self.fail('Finish the test!')

        # The page updates again, and now shows both items on her list

# if __name__ == '__main__':
#     unittest.main(warnings='ignore')